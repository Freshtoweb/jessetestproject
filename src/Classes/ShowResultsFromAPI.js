/**
 * This React class is intended to query an endpoint that will return an alphanumeric string, after clicking a button.
 * This component is passed a prop "apiQueryDelay", which delays the endpoint request by N milliseconds. There is a
 * second button to disable this functionality and have the endpoint request run immediately after button click.
 * This data is then to be displayed inside a simple container.
 * The "queryAPI" XHR handler will return the endpoint response in the form of a Promise (such as axios, fetch).
 * The response object will look like the following: {data: "A0B3HCJ"}
 * The containing element ref isn't used, but should remain within the class.
 * Please identify, correct and comment on any errors or bad practices you see in the React component class below.
 * Additionally, please feel free to change the code style as you see fit.
 * Please note - React version for this exercise is 15.5.4
 */


// Added import for correct module component from react
import React, { Component } from 'react';


// Im typically a visual debugger, but the unresolved queryAPI forced me to do this from a high level review

// Commented queryAPI
//import queryAPI from 'queryAPI';

// Created test async function
var queryAPI = async function(){
    return {data: "A0B3HCJ"};
};

// Defined export statement here
// Removed parenthesis
export default class ShowResultsFromAPI extends Component {
    constructor(props) {
        super(props);
        // IDK I like to think of props as being immutable
        // Removed unused container variable
        this.state = {
            apiQueryDelay: 1
        };
    }

    toggleDelay() {
        // changed to toggle delay instead of disable
        this.setState({
            apiQueryDelay: (this.state.apiQueryDelay == 1 ? 0 : 1)
        });
    }

    click() {
        // Function would not fire if apiquerydelay was not set, so removed it
        // arrow function cleans up syntax slightly
        // Delay wouldnt make sense as 1 == 0.001
        // Hardcoded 1 second instead
        setTimeout(
            () => this.fetchData(),
            1000 * this.state.apiQueryDelay
        );

    }

    fetchData() {
        // Added arrow func
        // Creates error state
        queryAPI()
            .then((response) => {
                var error = (response.data) ? false : true;
                this.setState({
                    data: response.data,
                    error: error
                });

            });
    }

    render() {
        // Toggle delay instead of disable
        // corrected output ternary statement
        // Removed unused reference id
        // corrected onClicks to match scope, removes bind
        // Corrected Button => button, Button is only in react native when imported correctly
        // class => className
        return (
            <div className="content-container" ref="container">
                <p>
                    {
                        (!this.state.error)
                            ? this.state.data
                            : "Sorry - there was an error with your request."
                    }
                </p>
                <button onClick={() => this.toggleDelay()}>Toggle delay</button>
                <button onClick={() => this.click()}>Request data from endpoint</button>
            </div>
        );
    }
}

// I wouldve merged these into a singular object, I removed them because I didn't see them referenced in a constructor

// Removed export statement